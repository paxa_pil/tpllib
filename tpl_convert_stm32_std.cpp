#include "tpl_convert_stm32_std.h"

//namespace SplConvertion  {


#define _NUM_OF_DEV(d,start) ((d-start)/0x0400)
//return freq of apb, ahb
uint32_t Tpl_initDevClock(void* device)
{
    void (*RCC_PeriphClockCmd)(uint32_t, FunctionalState) = nullptr;
    uint32_t dev = (uint32_t)device;
    uint32_t freq = 0;
    uint8_t n_dev;

    static bool first=true;
    static RCC_ClocksTypeDef clocks;
    if(first) {
        first=false;
        RCC_GetClocksFreq(&clocks);
    }

    #if !defined(STM32F10X_CL)
    if( dev >= AHB2PERIPH_BASE )    {
        n_dev = _NUM_OF_DEV( dev, AHB2PERIPH_BASE);
        RCC_PeriphClockCmd = RCC_AHB2PeriphClockCmd;
        freq = clocks.HCLK_Frequency;
    }
    else if( dev >= AHB1PERIPH_BASE )   {
        n_dev = _NUM_OF_DEV( dev, AHB1PERIPH_BASE);
        if(dev>=DMA1_BASE)
            n_dev-= 3;
        RCC_PeriphClockCmd = RCC_AHB1PeriphClockCmd;
        freq = clocks.HCLK_Frequency;
    }
    #elif defined(STM32F10X_CL)
    if( dev >= AHBPERIPH_BASE )
    {
        n_dev = _NUM_OF_DEV( dev, AHBPERIPH_BASE);
        RCC_PeriphClockCmd = RCC_AHBPeriphClockCmd;
        freq = clocks.HCLK_Frequency;
    }
    #endif

    else if( dev >= APB2PERIPH_BASE )   {
        n_dev = _NUM_OF_DEV( dev, APB2PERIPH_BASE);
        #if !defined(STM32F10X_CL)
        if( dev>= ADC1_BASE && dev<= ADC3_BASE)
            n_dev+= ((dev-(uint32_t)ADC1)/0x100);
        #endif
        RCC_PeriphClockCmd = RCC_APB2PeriphClockCmd;
        freq = clocks.PCLK2_Frequency;
    }
    else if( dev >= APB1PERIPH_BASE )   {
        n_dev = _NUM_OF_DEV( dev, APB1PERIPH_BASE);
        RCC_PeriphClockCmd = RCC_APB1PeriphClockCmd;
        freq = clocks.PCLK1_Frequency;
    }

    RCC_PeriphClockCmd(0x00000001<<n_dev, ENABLE);

    return(freq);
}



uint8_t Tpl_sourceFromPin(uint16_t pin)
{
	uint16_t mask = 0x0001;
	int i_pin;
	for(i_pin=0; i_pin<16; i_pin++)	{
		if( mask & pin )
			return(GPIO_PinSource0 + i_pin );
		mask = mask<<1;
	}
	return(0);
}


uint8_t Tpl_irqEnable( IRQn_Type IRQn, tIrqSettings settings )
{
#if defined(STM32F30X)
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = settings.PreemptionPriority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = settings.SubPriority;
    NVIC_InitStructure.NVIC_IRQChannelCmd = settings.on?ENABLE:DISABLE;
    NVIC_Init(&NVIC_InitStructure);
    return(1);

#elif defined(STM32F4XX) || defined(STM32F2XX)
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = settings.PreemptionPriority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = settings.SubPriority;
    NVIC_InitStructure.NVIC_IRQChannelCmd = settings.on?ENABLE:DISABLE;
    NVIC_Init(&NVIC_InitStructure);
    return(1);

#elif defined(STM32F1XX)
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = settings.PreemptionPriority;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = settings.SubPriority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = settings.on?ENABLE:DISABLE;
	NVIC_Init(&NVIC_InitStructure);
	return(1);
#else
	#error "No func TPL_Dev_IRQn_Enable for this stm32"
    return(0);
#endif
}

uint8_t Tpl_getExtiPortSource( GPIO_TypeDef *port)
{
    return( 0x0F & (((uint32_t)port)>>10 ) );
}

//}   //namespace


#ifndef __TPL_TIMER_H__
#define __TPL_TIMER_H__

#include "tpl_std_incl.h"

typedef struct {
    uint32_t freq;
    uint16_t countMode; //TIM_CounterMode_Up , TIM_CounterMode_Down ...
    uint32_t period;
    uint16_t division;  //TIM_CKD_DIV1, TIM_CKD_DIV2, TIM_CKD_DIV4
    uint8_t repetitionCounter;
}tTimer;

//example:
// { .freq=1000, .countMode=TIM_CounterMode_Up, .period=1000, .division=TIM_CKD_DIV1, .repetitionCounter=0}


class TplTimer {
    public:
        TIM_TypeDef *tim;
        TplTimer()  {};
        TplTimer( TIM_TypeDef *_tim, tTimer settings)
        {
            init(_tim, settings);
        }
        void init(TIM_TypeDef *_tim, tTimer settings);
        void start(bool on=true)
        {
            TIM_Cmd(tim, on?ENABLE:DISABLE);
        }
        void preloadConfigARR(bool on=true)
        {
            TIM_ARRPreloadConfig(tim, on?ENABLE:DISABLE);
        }
        void ctrlPWMOutputs( bool on=true)
        {
            TIM_CtrlPWMOutputs(tim, on?ENABLE:DISABLE);
        }
        void setAutoreload( uint32_t Autoreload)
        {
            TIM_SetAutoreload(tim, Autoreload);
        }
        void selectOnePulseMode(bool on=true)
        {
            TIM_SelectOnePulseMode( tim, on?TIM_OPMode_Single:TIM_OPMode_Repetitive);
        }
        void itConfig(uint16_t TIM_IT, bool on=true)
        {
           TIM_ITConfig( tim, TIM_IT, (FunctionalState)on);
        }
        void clearItPendingBit(uint16_t TIM_IT)
        {
            TIM_ClearITPendingBit( tim, TIM_IT);
        }
        uint8_t getAF(void);
    private:
        uint8_t m_af;
};

#define _TPL_TIM_CHANNELS_N     4

typedef void (*tTIM_OCxInit)(TIM_TypeDef* TIMx,
                                         TIM_OCInitTypeDef* TIM_OCInitStruct);
const tTIM_OCxInit _TIM_OCnInit[_TPL_TIM_CHANNELS_N] = { TIM_OC1Init, TIM_OC2Init,
                                            TIM_OC3Init, TIM_OC4Init };
typedef void (*tTIM_OCxPreloadConfig)(TIM_TypeDef* TIMx,
                                         uint16_t TIM_OCPreload);
const tTIM_OCxPreloadConfig _TIM_OCnPreloadConfig[_TPL_TIM_CHANNELS_N] =
        { TIM_OC1PreloadConfig, TIM_OC2PreloadConfig,
        TIM_OC3PreloadConfig, TIM_OC4PreloadConfig };

#if !defined(STM32F1XX)
typedef void (*tTIM_SetCompareX)(TIM_TypeDef* TIMx, uint32_t Compare2);
#else
typedef void (*tTIM_SetCompareX)(TIM_TypeDef* TIMx, uint16_t Compare2);
#endif
const tTIM_SetCompareX _TIM_SetCompareN[_TPL_TIM_CHANNELS_N] =
        { TIM_SetCompare1, TIM_SetCompare2,
        TIM_SetCompare3, TIM_SetCompare4 };

class TplTimerChannel {
    public:
        TplTimerChannel()   {};
        TplTimerChannel( TplTimer *timer, uint8_t channel, TIM_OCInitTypeDef *initStructOC  )
        {
            init( timer, channel);
            initOC( initStructOC );
        }
        TplTimerChannel( TplTimer *timer, uint8_t channel )
        {
            init( timer, channel);
        }
        int init( TplTimer *timer, uint8_t channel );
        void preloadConfigOC( bool on=true )
        {
            m_OCxPreloadConfig( m_timer->tim,
                               on?TIM_OCPreload_Enable:TIM_OCPreload_Disable);
        }
        int initOC( TIM_OCInitTypeDef *initStructOC )
        {
            m_OCxInit( m_timer->tim, initStructOC);
        }
        int setCompare( uint32_t compare )
        {
            m_SetCompareX( m_timer->tim, compare);
        }
    private:
        TplTimer  *m_timer;
        //uint8_t m_n;
        tTIM_OCxInit m_OCxInit;
        tTIM_OCxPreloadConfig  m_OCxPreloadConfig;
        tTIM_SetCompareX m_SetCompareX;
};

#endif /* __TPL_TIMER_H__ */

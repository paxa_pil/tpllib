#ifndef __TPL_RS485_H__
#define __TPL_RS485_H__

#include "tpl_usart.h"
#include "tpl_pinout.h"
#include "tpl_std_incl.h"
#include "tpl_roundbuffer.h"

//typedef struct
//{
//    USART_TypeDef * n;
//    hwPin  tx;
//    hwPin  rx;
//}hwUsart;

class TplRS485
{
	public:
	    TplRoundBuffer rb_in, rb_out;

	    TplRS485(hwUsart usart, USART_InitTypeDef settings, uint32_t sizes, hwPin pinDE);
        uint8_t inithw(hwUsart usart, USART_InitTypeDef settings);
        IRQn_Type getIRQn(void);
        uint8_t getAF(void);

        void set_irqTXE(bool en)
		{
			USART_ITConfig(nameUsart,  USART_IT_TXE, en?ENABLE:DISABLE);
		}

		void set_irqRXNE(bool en)
		{
			USART_ITConfig(nameUsart,  USART_IT_RXNE, en?ENABLE:DISABLE);
		}

		void set_irqTC(bool en)
		{
			USART_ITConfig(nameUsart,  USART_IT_TC, en?ENABLE:DISABLE);
		}

		void clear_irqTC(void)
		{
			USART_ClearITPendingBit(nameUsart, USART_IT_TC);
		}

		bool get_irqRXNE(void)
		{
			return( (nameUsart->SR&USART_SR_RXNE)&&(nameUsart->CR1&USART_CR1_RXNEIE) );
		}

		bool get_irqTXE(void)
		{
			//return((bool)USART_GetITStatus(nameUsart , USART_IT_TXE));
			return( (nameUsart->SR&USART_SR_TXE)&&(nameUsart->CR1&USART_CR1_TXEIE) );
		}

		bool get_irqTC(void)
		{
			//return((bool)USART_GetITStatus( nameUsart , USART_IT_TC));
			return( (nameUsart->SR&USART_SR_TC)&&(nameUsart->CR1&USART_CR1_TCIE) );
		}

		void start_send(void)
		{
		    usartDE.set(true);
			set_irqTXE(true);
		}




#if defined(STM32F30X)

		uint8_t get_byte(void)
		{
			return(nameUsart->RDR & (uint8_t)0xFF);
		}

		void put_byte(uint8_t byte)
		{
			nameUsart->TDR = byte;
		}

#elif defined(STM32F10X_MD) || defined(STM32F2XX) || defined(STM32F4XX)

		uint8_t get_byte(void)
		{
			return(nameUsart->DR & (uint8_t)0xFF);
		}

		void put_byte(uint8_t byte)
		{
			nameUsart->DR = (byte & (uint16_t)0x01FF);
		}

#else
		#error "Error in tpl_usart.h for this dev"
#endif

        void receive(void)
		{
		    if(get_irqRXNE())
				rb_in.push(get_byte());
		}

		void pushByte(void)
		{
		    rb_in.push(get_byte());
		}

		void transmit(void)
		{
			if(get_irqTXE())
			{
				if(rb_out.getCount())
					put_byte(rb_out.pop());
				else
				{
					set_irqTXE(false);
					set_irqTC(true);
				}
			}

			if(get_irqTC())
			{
				usartDE.set(false);
				set_irqTC(false);
			}
		}
		__attribute__((always_inline)) void inirq(void)
		{
		    if(get_irqRXNE())
				rb_in.push(get_byte());

		    if(get_irqTXE())
			{
				if(rb_out.getCount())
					put_byte(rb_out.pop());
				else
				{
					set_irqTXE(false);
					set_irqTC(true);
				}
			}

			if(get_irqTC())
			{
				usartDE.set(false);
				set_irqTC(false);
			}
		}

    private:
        USART_TypeDef  *nameUsart;
		IRQn_Type       m_irqn;
		uint8_t         m_af;
		TplPin          usartDE;

};


#endif //__TPL_RS485_H__

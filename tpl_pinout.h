#ifndef __TPL_PINOUT_H__
#define __TPL_PINOUT_H__

#include "tpl_std_incl.h"
#include "tpl_ports_def.h"
#include "tpl_convert_stm32_std.h"
/*


*/
//example
//pwm pa6,
//#define KI_PWM1  PA6


#if (defined(STM32F30X) || defined(STM32F4XX) || defined(STM32F2XX))
	typedef struct structPinoutSettings {
		GPIOMode_TypeDef mode;
		GPIOPuPd_TypeDef pupd;
		GPIOSpeed_TypeDef speed;
		GPIOOType_TypeDef otype;
	} PinSettings;

    #define PIN_SET_OUT_PP_PUP      {GPIO_Mode_OUT, GPIO_PuPd_UP,     STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_OUT_PP_PDOWN    {GPIO_Mode_OUT, GPIO_PuPd_DOWN,   STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_OUT_PP_NOPULL   {GPIO_Mode_OUT, GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP}

    #define PIN_SET_OUT_OD_PUP      {GPIO_Mode_OUT, GPIO_PuPd_UP,     STD_GPIO_MaxSpeed, GPIO_OType_OD}
    #define PIN_SET_OUT_OD_PDOWN    {GPIO_Mode_OUT, GPIO_PuPd_DOWN,   STD_GPIO_MaxSpeed, GPIO_OType_OD}
    #define PIN_SET_OUT_OD_NOPULL   {GPIO_Mode_OUT, GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_OD}

    #define PIN_SET_AF_PP_PUP       {GPIO_Mode_AF,  GPIO_PuPd_UP,     STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_AF_PP_PDOWN     {GPIO_Mode_AF,  GPIO_PuPd_DOWN,   STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_AF_PP_NOPULL    {GPIO_Mode_AF,  GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP}

    #define PIN_SET_AF_OD_PUP       {GPIO_Mode_AF,  GPIO_PuPd_UP,     STD_GPIO_MaxSpeed, GPIO_OType_OD}
    #define PIN_SET_AF_OD_PDOWN     {GPIO_Mode_AF,  GPIO_PuPd_DOWN,   STD_GPIO_MaxSpeed, GPIO_OType_OD}
    #define PIN_SET_AF_OD_NOPULL    {GPIO_Mode_AF,  GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_OD}

    #define PIN_SET_IN_PDOWN        {GPIO_Mode_IN,  GPIO_PuPd_DOWN,   STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_IN_PUP          {GPIO_Mode_IN,  GPIO_PuPd_UP,     STD_GPIO_MaxSpeed, GPIO_OType_PP}
    #define PIN_SET_IN_NOPULL       {GPIO_Mode_IN,  GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP}

    #define PIN_SET_AN              {GPIO_Mode_AN,  GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP}

    // legacy support
    //#define PIN_SET_OUT  {GPIO_Mode_OUT, GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP}
    //#define PIN_SET_AF  {GPIO_Mode_AF, GPIO_PuPd_NOPULL, STD_GPIO_MaxSpeed, GPIO_OType_PP }
    //#define PIN_SET_AF_TIMOC  {GPIO_Mode_AF, GPIO_PuPd_UP, STD_GPIO_MaxSpeed, GPIO_OType_PP }
    #define PIN_SET_OUT PIN_SET_OUT_PP_NOPULL
    #define PIN_SET_AF  PIN_SET_AF_PP_NOPULL
    #define PIN_SET_AF_TIMOC PIN_SET_AF_OD_PUP
    // legacy support

#elif defined(STM32F1XX)

	typedef struct structPinoutSettings {
		GPIOMode_TypeDef mode;
		GPIOSpeed_TypeDef speed;
	} PinSettings;

	const PinSettings PIN_SET_OUT = {GPIO_Mode_Out_PP, STD_GPIO_MaxSpeed };
	const PinSettings PIN_SET_AF_OD = {GPIO_Mode_AF_OD, STD_GPIO_MaxSpeed  };
	const PinSettings PIN_SET_AF_PP = {GPIO_Mode_AF_PP, STD_GPIO_MaxSpeed  };
	const PinSettings PIN_SET_AN = {GPIO_Mode_AIN, STD_GPIO_MaxSpeed  };
	const PinSettings PIN_SET_IN_PDOWN = {GPIO_Mode_IPD, STD_GPIO_MaxSpeed  };
	const PinSettings PIN_SET_IN_PUP = {GPIO_Mode_IPU, STD_GPIO_MaxSpeed  };
	const PinSettings PIN_SET_IN_NOPULL = {GPIO_Mode_IN_FLOATING, STD_GPIO_MaxSpeed  };
#else
	#error "No typedef PinSettings for this stm32"
#endif

class TplPin {
	private:
	hwPin mPin;
    IRQn_Type m_irqn;
	public:
	TplPin( hwPin pin, const PinSettings& pinSettings )
	{
		init( pin, pinSettings ) ;
	}

	TplPin( hwPin pin ):
		mPin(pin)
	{ }

	TplPin(void) {}

	void init( hwPin pin, const PinSettings& pinSettings )
	{
		GPIO_InitTypeDef  GPIO_InitStructure;
		mPin=pin;
        m_irqn = (IRQn_Type)_IRQ_STARTUP;

        Tpl_initDevClock( mPin.port );

		#if defined(STM32F30X) || defined(STM32F4XX) || defined(STM32F2XX)
			GPIO_InitStructure.GPIO_Pin = mPin.pin;
			GPIO_InitStructure.GPIO_Mode = pinSettings.mode;
			GPIO_InitStructure.GPIO_OType = pinSettings.otype;
			GPIO_InitStructure.GPIO_PuPd = pinSettings.pupd;
			GPIO_InitStructure.GPIO_Speed = pinSettings.speed;
		#elif defined(STM32F1XX)
			GPIO_InitStructure.GPIO_Pin = mPin.pin;
			GPIO_InitStructure.GPIO_Mode = pinSettings.mode;
			GPIO_InitStructure.GPIO_Speed = pinSettings.speed;
		#endif
		GPIO_Init(mPin.port, &GPIO_InitStructure);
	}

    #if !defined(STM32F1XX)
    void setAF( uint8_t af)
    {
        GPIO_PinAFConfig(mPin.port, Tpl_sourceFromPin(mPin.pin), af);
    }
    #else
    void setRemap(uint32_t GPIO_Remap, FunctionalState NewState)
    {
        GPIO_PinRemapConfig(GPIO_Remap, NewState);
    }
    #endif

	bool get( void )
	{
		 return( (bool)(mPin.port->IDR & mPin.pin)  ) ;
	}

	#if defined(STM32F30X) || defined(STM32F1XX)
		void up(void)
		{
			((uint16_t*)&(mPin.port->BSRR))[0] = mPin.pin;
		}
		void down(void)
		{
			((uint16_t*)&(mPin.port->BRR))[1] = mPin.pin;
		}
	#elif defined(STM32F4XX) || defined(STM32F2XX)
		void up(void)
		{
			mPin.port->BSRRL = mPin.pin;
		}
		void down(void)
		{
			mPin.port->BSRRH = mPin.pin;
		}
	#else
		#error "No func's TplPin::up and CPL_Pin::down for this stm32"
	#endif

	void set( bool input )
	{
		if(input)
			up();
		else
			down();
	}

	void toggel(void)
	{
		mPin.port->ODR ^= mPin.pin;
	}

    #if !defined(STM32F1XX) && !defined(STM32F4XX)
    void initExtiIrq( EXTITrigger_TypeDef trigger, tIrqSettings settingsIrq = DEFAULT_IRQ_SETTINGS )
    {
        Tpl_initDevClock(SYSCFG);

        SYSCFG_EXTILineConfig( Tpl_getExtiPortSource(mPin.port), Tpl_sourceFromPin(mPin.pin));
        EXTI_InitTypeDef   EXTI_InitStructure;

        EXTI_InitStructure.EXTI_Line = mPin.pin;
        EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_Trigger = trigger;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_Init(&EXTI_InitStructure);

        Tpl_irqEnable( getIRQn(), settingsIrq);
    }
    #endif

    IRQn_Type getIRQn(void)
    {
        if(m_irqn == _IRQ_STARTUP)   {
            if( mPin.pin == GPIO_Pin_0 )
                m_irqn = EXTI0_IRQn;
            else if( mPin.pin == GPIO_Pin_1 )
                m_irqn = EXTI1_IRQn;
            else if( mPin.pin == GPIO_Pin_2 )
                m_irqn = EXTI2_IRQn;
            else if( mPin.pin == GPIO_Pin_3 )
                m_irqn = EXTI3_IRQn;
            else if( mPin.pin == GPIO_Pin_4 )
                m_irqn = EXTI4_IRQn;
            else if( mPin.pin >= GPIO_Pin_5 && mPin.pin <= GPIO_Pin_9)
                m_irqn = EXTI9_5_IRQn;
            else if( mPin.pin >= GPIO_Pin_10 && mPin.pin <= GPIO_Pin_15)
                m_irqn = EXTI15_10_IRQn;
            else    //err
                m_irqn = (IRQn_Type)0;
        }
        return( m_irqn );
    }

    bool getStatusIT(void)
    {
        return( (EXTI->PR & mPin.pin) && (EXTI->IMR & mPin.pin) );
    }

    void clearStatusIT(void)
    {
        EXTI->PR = mPin.pin;
    }
};

#endif //__TPL_PINOUT_H__


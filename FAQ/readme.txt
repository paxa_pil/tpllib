1.Добавить в файл линковщика (например stm32f207vc_flash.ld) объявление bss

БЫЛО:
	.bss (NOLOAD):
	{
		__bss_start__ = .;
		*(.bss*)
		*(COMMON)
		__bss_end__ = .;
	} > RAM
СТАЛО
	.bss (NOLOAD):
	{
		__bss_start__ = .;
		*(.bss*)
		*(COMMON)
		_ebss = . ;
		__bss_end__ = _ebss;
	} > RAM

2. добавить в проект newlib_stubs.c


Warnings
1. like  warning: extended initializer lists only available with -std=c++11 or -std=gnu++11 [enabled by default]|
	select GNU++11 standard c++
2. like warning: command line option '-std=gnu11' is valid for C/ObjC but not for C++ [enabled by default]|
	set same compiler for .c and .cpp files => g++ 
#include "tpl_roundbuffer.h"

TplRoundBuffer::TplRoundBuffer(uint32_t size):
	mbuf(nullptr)
{
	init(size);
}

void TplRoundBuffer::init(uint32_t size)
{
	delete [] mbuf;
	mbuf = new uint8_t[size];
	msize = size;
	clear();
}

void TplRoundBuffer::init(void *buffer, uint32_t size)
{
	delete [] mbuf;
	mbuf = (uint8_t*)buffer;
	msize = size;
	clear();
}

void TplRoundBuffer::clear(void)
{
	mtail = 0;
	mhead = 0;
}

bool TplRoundBuffer::removeRange(uint32_t count)
{
	if(count >= getCount())
	{
		clear();
		return(false);
	}
	mrb_plus(&mhead, count);
	return(true);
}

uint32_t TplRoundBuffer::getCount(void)
{
	if( mtail >= mhead)
		return(mtail - mhead);
	return(msize - mhead + mtail);
}

uint32_t TplRoundBuffer::getCount(uint32_t index)
{
	if( index >= mhead)
		return(index - mhead);
	return(msize - mhead + index);
}

uint8_t TplRoundBuffer::operator [](uint32_t index)
{
	uint32_t pos = mhead;
	mrb_plus(&pos, index);

	return(mbuf[pos]);
}

uint16_t TplRoundBuffer::getWordUnsafe(uint32_t index)
{
	if((index + 1) >= getCount())
		return(0);
	return((*this)[index] + ((uint16_t)(*this)[index + 1] << 8));
}

uint32_t TplRoundBuffer::get(void *resultBuffer, uint32_t index, uint32_t count)
{
	uint32_t cnt = getCount();
	uint32_t cnt_out = ((count + index) <= cnt) ? (count + index) : cnt;
	for( uint32_t i = index; i < cnt_out; i++)
		((uint8_t*)resultBuffer)[i - index] = (*this)[i];
	return(cnt_out);
}

bool TplRoundBuffer::push(uint8_t byte)
{
	mbuf[mtail] = byte;
	mrb_plus(&mtail, 1);

	if(mtail == mhead)
	{
		mrb_plus( &mhead, 1);
		return(false);
	}

	return(true);
}

bool TplRoundBuffer::push(void *buffer, uint32_t count)
{
	bool result = true;

	for(uint32_t i = 0; i < count; i++)
		result = push(((uint8_t*)buffer)[i]);
	return result;
}

bool TplRoundBuffer::push(uint32_t index, uint8_t byte)
{
	if (index > msize) return(false);
	mbuf[index] = byte;
	return(true);
}

bool TplRoundBuffer::push(TplRoundBuffer *tplRoundBuffer, uint32_t count)
{
	bool result = true;
	uint32_t max_size = tplRoundBuffer->getCount();

	max_size = ((max_size<count) ? max_size : count);

	for(uint32_t i = 0; i < max_size; i++)
		result = push(tplRoundBuffer->pop());

	return result;
}

uint8_t TplRoundBuffer::pop(void)
{
	if(!getCount())
		return(0);
	uint8_t byte = mbuf[mhead];
	mrb_plus(&mhead, 1);
	return(byte);
}

uint8_t TplRoundBuffer::popUnsafe(void)
{
	uint8_t byte = mbuf[mhead];
	mrb_plus(&mhead, 1);
	return(byte);
}

uint32_t TplRoundBuffer::pop(void *resultBuffer, uint32_t count)
{
	uint32_t cnt = getCount();
	uint32_t cnt_out = (count <= cnt) ? count : cnt;

	for(uint32_t i = 0; i < cnt_out; i++)
		((uint8_t*)resultBuffer)[i] = pop();

	return(cnt_out);
}

int TplRoundBuffer::memcmp(  void *buffer, uint32_t sizebuf)
{
	uint8_t *buf = (uint8_t*)buffer;
	unsigned int cnt = getCount();

	if( !cnt || !sizebuf || ( sizebuf > cnt ) )
		return(-1);

	uint32_t i=0;
	while( i < sizebuf && buf[i] == (*this)[i] )
		i++;
	if( i == sizebuf )
		return(0);	//???????
	return(i+1);
}

int TplRoundBuffer::memSearch(  void *buffer, uint32_t sizebuf)
{
	uint32_t cnt =getCount();
	if( cnt < sizebuf )
		return(-1);
	uint8_t *pbuf = (uint8_t*)buffer;
	for( uint32_t i_begin=0; i_begin<=(cnt-sizebuf); i_begin++) {
		uint32_t i_word;
		for(i_word=0; i_word<sizebuf; i_word++) {
			uint8_t byte = (*this)[i_begin+i_word];
			if( byte != pbuf[i_word] ) {
				if( i_word !=0)
					i_word = 0;
				break;
			}
		}
		if( i_word == sizebuf )
			return( i_begin);
	}
	return(-2);
}

int32_t TplRoundBuffer::findPackage(uint8_t *startPattern, uint8_t sizePattern, uint32_t sizePackage)
{
    if (sizePackage<2) return -1;
    if (sizePattern<1) return -2;
    if (getCount()<sizePackage) return -3;

    for (uint32_t pBuffer = mhead, i=0; i < getCount(); (pBuffer<msize)?i++, pBuffer++:pBuffer=0,i++)
    {
        for (uint32_t j=0; j<sizePattern; j++)
        {
            if (startPattern[j]!=mbuf[pBuffer+j])
            {
                mhead=pBuffer;
                break;
            }

            if (j==sizePattern-1)
            {
                mhead=pBuffer;
                return mhead;
            }
        }
    }
    return -4;
}

void TplRoundBuffer::printf(char *format, ...)
{
	char buf_printf[TPL_PRINTF_MAX_LENGHT];
	va_list vap;
	va_start(vap, format);
	vsprintf(buf_printf, format, vap);
	va_end(vap);
    push(buf_printf, strlen(buf_printf)+1);
}

char* TplRoundBuffer::printfBuffer(char *str)
{
	if(!getCount())
		str[0] = '\0';
		//sprintf(str, " ");
	else
		for(uint32_t i=0; i<getCount(); i++)
			sprintf(str + i*3,"%02X ", (*this)[i]);
	return(str);
}

char* PL_PrintfBuffer(void *buf, uint8_t buf_len, char *str)
{
	int i;
	uint8_t *pbuf = (uint8_t*)buf;
	for(i=0; i<buf_len; i++)
		sprintf(str + i*3,"%02X ", pbuf[i]);
	return(str);
}

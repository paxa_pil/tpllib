#ifndef __TPL_GETTIME_H__
#define __TPL_GETTIME_H__

#include "tpl_std_incl.h"
#include "tpl_pinout.h"

#define MAX_TIMER_SUM   50

#define us_in_ms        1000

typedef enum enum_type_timer { TPL_CYCLE_TIMER=1, TPL_ONE_TIMEOUT }tTimerType;

typedef struct timer_struct {
    uint8_t type;
    uint32_t SaveTime;
    uint32_t TimeOut;
    uint8_t run;
}sTimer;

extern volatile uint32_t cnt_ms_global;

class TplTimer_ms {
	private:
		tTimerType m_type;
		uint32_t m_cnt_save;
		uint32_t m_timeout_save;
		bool m_run;
	public:
		void init( tTimerType type_in, uint32_t timeout_in );
		bool timeout(void);
		bool start(void);
		bool stop(void);
		inline void start_one( uint32_t tin)
		{
			m_cnt_save = cnt_ms_global;
			m_timeout_save = tin;
			m_run = true;
		}
};

void TplInit_GetTime_SysTick(void);

void TplResetTime(void);
uint32_t TplGetTime_ms(void);
void delay_ms(uint16_t milliseconds);
//extern "C"  {
extern "C"  void delay_nop( uint32_t nCount);

extern "C"	void SysTick_Handler(void);


//}

#endif //__TPL_GETTIME_H__

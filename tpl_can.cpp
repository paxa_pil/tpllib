#include "tpl_can.h"
#include "tpl_pinout.h"
#include <string.h>

TplCan::TplCan(HwCan hw){

    /* Default configuration */
    /* CAN Baud-rate = 125kbps (CAN clocked at 42 MHz) */
    /* Sample-Point at 87.5 % is the preferred value used by CANopen and DeviceNet */
    ConfCan confCan;
    confCan.SJW = CAN_SJW_1tq;
    confCan.BS1 = CAN_BS1_13tq;
    confCan.BS2 = CAN_BS2_2tq;
    confCan.Prescaler = 21;

    mconf = confCan;

    doInitialize(hw, mconf);
}

TplCan::TplCan(HwCan hw, ConfCan conf):
    mconf(conf)
{
    doInitialize(hw, mconf);
}
uint8_t TplCan::doInitialize(HwCan hw, ConfCan conf)
{
    mhw = hw;

    TplPin mtx(mhw.tx, PIN_SET_AF_OD_PUP);
    TplPin mrx(mhw.rx, PIN_SET_AF_OD_NOPULL);


    mtx.setAF(GPIO_AF_CAN);
    mrx.setAF(GPIO_AF_CAN);

    Tpl_initDevClock(mhw.n);
    CAN_DeInit(mhw.n);

    if (mhw.n == CAN1){
    Tpl_irqEnable(CAN1_RX0_IRQn);
    Tpl_irqEnable(CAN1_RX1_IRQn);

    }else if (mhw.n == CAN2){
    Tpl_initDevClock(CAN1);//can2 is slave of can1. It works with master can1.
    CAN_DeInit(CAN1);

    Tpl_irqEnable(CAN2_RX0_IRQn);
    Tpl_irqEnable(CAN2_RX1_IRQn);
    }

    /* CAN cell init */
    CAN_InitTypeDef CAN_InitStructure;
    CAN_StructInit(&CAN_InitStructure);
    CAN_InitStructure.CAN_SJW = mconf.SJW;
    CAN_InitStructure.CAN_BS1 = mconf.BS1;
    CAN_InitStructure.CAN_BS2 = mconf.BS2;
    CAN_InitStructure.CAN_Prescaler = mconf.Prescaler;
    CAN_Init(mhw.n, &CAN_InitStructure);

    /* Enable FIFO 0 message pending Interrupt */
    CAN_ITConfig(mhw.n, CAN_IT_FMP0, ENABLE);
    CAN_ITConfig(mhw.n, CAN_IT_FMP1, ENABLE);

    return 0;
}

int8_t TplCan::doSend(FrameCan frame)
{
    if ((frame.id < MIN_ID) || (frame.id > MAX_ID) || (frame.length > MAX_DATA))
        return -1;
    CanTxMsg TxMessage;

    TxMessage.StdId = frame.id;
    TxMessage.ExtId = 0;
    TxMessage.IDE = CAN_Id_Standard;
    TxMessage.RTR = CAN_RTR_Data;
    TxMessage.DLC = frame.length;

    memcpy(TxMessage.Data, frame.data, frame.length);

    CAN_Transmit(mhw.n, &TxMessage);

    return 0;
}

FrameCan TplCan::doReceive(CAN_TypeDef *can, uint8_t fifo)
{
    CanRxMsg RxMessage;
    FrameCan frame;

    CAN_Receive(can, fifo, &RxMessage);
    frame.id = RxMessage.StdId;
    frame.length = RxMessage.DLC;
    memcpy(&frame.data, RxMessage.Data, frame.length);

    return frame;
}

void TplCan::setFilter(FilterCan filter){
    typedef struct filter16bit
    {
                                //Position in register
        uint8_t extIdBits15_17; //[0:2]
        bool ide;               //[3]
        bool rtr;               //[4]
        uint16_t stdId;         //[5:15]
        uint16_t assembleRegister()
        {
            uint16_t reg = 0;
            reg |= (extIdBits15_17) | (ide << 3) | (rtr << 4) | (stdId << 5);
            return reg;
        }
    } Filter16bit;

//    typedef struct filter32bit
//    {
//                                //Position in register
//        bool rtr;               //[1]
//        bool ide;               //[2]
//        uint8_t extId;          //[3:20]
//        uint16_t stdId;         //[21:31]
//        uint16_t assembleRegister()
//        {
//            uint32_t reg = 0;
//            reg |= (rtr << 1) | (ide << 2) | (extId << 3) | (stdId << 21);
//            return reg;
//        }
//    } Filter32bit;

    Filter16bit id
    {
        .extIdBits15_17 = 0,
        .ide = CAN_Id_Standard,
        .rtr = CAN_RTR_DATA,
        .stdId = (uint16_t)filter.id
    };

    Filter16bit mask
    {
        .extIdBits15_17 = 0,
        .ide = 0,
        .rtr = 0,
        .stdId = 0x7FF //11 bit mask
    };

    CAN_FilterInitTypeDef  CAN_FilterInitStructure;

    CAN_FilterInitStructure.CAN_FilterNumber = filter.num;
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;

    CAN_FilterInitStructure.CAN_FilterIdLow = id.assembleRegister();
    CAN_FilterInitStructure.CAN_FilterMaskIdLow = mask.assembleRegister();

    CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;

    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = filter.fifo;
    CAN_FilterInitStructure.CAN_FilterActivation = filter.state;

    CAN_FilterInit(&CAN_FilterInitStructure);
}

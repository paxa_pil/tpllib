#include "tpl_adc.h"

void TplAdc::init(ADC_TypeDef *adc, ADC_InitTypeDef settings )
{
    m_adc = adc;

    ADC_Init(m_adc, &settings);
}

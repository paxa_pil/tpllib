#pragma once

#include "tpl_convert_stm32_std.h"

typedef struct 
{
    uint32_t channel;
    DMA_Stream_TypeDef* stream;
}hwDmaForAdc;

class TplAdcDma
{
public:
    TplAdcDma():
        mDma(nullptr), 
        mAdc(nullptr),
        mChannels(nullptr),
        mAdcData(nullptr)
    { }

    void init(ADC_TypeDef *adc, uint8_t *channels, uint8_t numChannels, hwDmaForAdc* pDma, bool regular=true);

    uint16_t getValue(uint8_t n)   
	{
        return(mAdcData[n]);
    }

    void enableTempSensor(void)
    {
        ADC_TempSensorVrefintCmd(ENABLE);
    }

    void start(void)
    {
         ADC_SoftwareStartConv(mAdc);
    }

private:
	uint8_t mNumChannels;
    hwDmaForAdc* mDma;
    ADC_TypeDef *mAdc;
    bool mRegular;
    uint8_t *mChannels;
    uint16_t *mAdcData;

    void configDma();
    void configAdc();
};
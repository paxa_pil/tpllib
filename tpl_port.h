#ifndef TPL_PORT_H_INCLUDED
#define TPL_PORT_H_INCLUDED

#include "tpl_pinout.h"

class TplPort {
private:
    GPIO_TypeDef* m_port;
    uint16_t m_mask;
public:
    TplPort(void)   {}
    TplPort( GPIO_TypeDef* port, uint16_t mask, PinSettings settings )
    {
        init( port, mask, settings);
    }
    void init( GPIO_TypeDef* port, uint16_t mask, PinSettings settings )
    {
        m_port = port;
        m_mask = mask;
        GPIO_InitTypeDef  GPIO_InitStructure;

        Tpl_initDevClock( m_port );

		#if defined(STM32F30X) || defined(STM32F4XX) || defined(STM32F2XX)
			GPIO_InitStructure.GPIO_Pin = m_mask;
			GPIO_InitStructure.GPIO_Mode = settings.mode;
			GPIO_InitStructure.GPIO_OType = settings.otype;
			GPIO_InitStructure.GPIO_PuPd = settings.pupd;
			GPIO_InitStructure.GPIO_Speed = settings.speed;
		#elif defined(STM32F10X_MD)
			GPIO_InitStructure.GPIO_Pin = m_mask;
			GPIO_InitStructure.GPIO_Mode = settings.mode;
			GPIO_InitStructure.GPIO_Speed = settings.speed;
		#endif
		GPIO_Init( m_port, &GPIO_InitStructure);
    }

    #if defined(STM32F30X) || defined(STM32F10X_MD)
		void up(uint16_t value)
		{
			((uint16_t*)&(m_port->BSRR))[0] = value;
		}
		void down(uint16_t value)
		{
			((uint16_t*)&(m_port->BSRR))[1] = value;GPIO_WriteBit
		}
	#elif defined(STM32F4XX) || defined(STM32F2XX)
		void up(uint16_t value)
		{
			m_port->BSRRL = value;
		}
		void down(uint16_t value)
		{
			m_port->BSRRH = value;
		}
	#else
		#error "No func's TplPin::up and CPL_Pin::down for this stm32"
	#endif
    void set( uint16_t value)
    {
        m_port->ODR = value;
    }

    void setMasked( uint16_t value)
    {
        down( (~value) & m_mask );
        up( value & m_mask);
    }
    uint16_t getMasked(void)
    {
        return(m_port->IDR & m_mask);
    }
    uint16_t get(void)
    {
        return( m_port->IDR );
    }
};



#endif /* TPL_PORT_H_INCLUDED */

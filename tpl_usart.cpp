#include "tpl_usart.h"
#include "tpl_pinout.h"

uint8_t TplUsart::init( hwUsart usart, USART_InitTypeDef settings,
                       uint32_t sizeIn, uint32_t sizeOut)
{
	rb_in.init( sizeIn );
	rb_out.init( sizeOut );
	return( inithw(usart, settings) );
}

uint8_t TplUsart::init( hwUsart usart, USART_InitTypeDef settings, uint8_t* buffer_in,
                        uint8_t* buffer_out, uint32_t buffer_size)
{
	rb_in.init( buffer_in, buffer_size );
	rb_out.init( buffer_out, buffer_size );
	return( inithw(usart, settings) );
}

uint8_t TplUsart::inithw( hwUsart usart, USART_InitTypeDef settings )
{
	nameUsart = usart.n;

    m_af = _AF_STARTUP;
    m_irqn = (IRQn_Type)_IRQ_STARTUP;

    #if defined(STM32F1XX)
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE);
    #endif


    if(!Tpl_initDevClock( nameUsart) )
        return(0);

	#if defined(STM32F30X) || defined(STM32F2XX) || defined(STM32F4XX)
		if(usart.rx.pin)    {
			TplPin rx( usart.rx, PIN_SET_AF );
			rx.setAF(getAF());
		}
		if(usart.tx.pin)    {
			TplPin tx( usart.tx, PIN_SET_AF );
			tx.setAF(getAF());
		}
	#elif defined(STM32F1XX)
        if(usart.rx.pin)
        {
            TplPin rx( usart.rx, PIN_SET_IN_NOPULL );

            if(usart.n == USART3)
            {
                if((usart.rx.port == GPIOC) & (usart.rx.pin = GPIO_Pin_11))
                    rx.setRemap(getPartialRemap(), ENABLE);
                else if((usart.rx.port == GPIOD) & (usart.rx.pin = GPIO_Pin_9))
                    rx.setRemap(getFullRemap(), ENABLE);
            }
            else rx.setRemap(getRemap(), ENABLE);
        }

        if(usart.tx.pin)
        {
            TplPin tx( usart.tx, PIN_SET_AF_PP );

            if(usart.n == USART3)
            {
                if((usart.rx.port == GPIOC) & (usart.rx.pin = GPIO_Pin_10))
                    tx.setRemap(getPartialRemap(), ENABLE);
                else if((usart.rx.port == GPIOD) & (usart.rx.pin = GPIO_Pin_8))
                    tx.setRemap(getFullRemap(), ENABLE);
            }
            else tx.setRemap(getRemap(), ENABLE);
        }

	#else
		 #error "No func TplUsart::init for this stm32"
 	#endif

	USART_Init(nameUsart, &settings);
  //  USART_ITConfig( Usart.USART_N, USART_IT_TXE | USART_IT_RXNE, ENABLE);
   USART_Cmd( nameUsart, ENABLE);
  // if(!PL_Dev_IRQn_Enable( usart.n ) )
   //     return(0);
	NVIC_EnableIRQ( getIRQn() );

   if(usart.rx.pin)
	    set_irqRXNE( true);
    return(1);
}


IRQn_Type TplUsart::getIRQn(void)
{
    if(m_irqn == _IRQ_STARTUP)   {
        if( nameUsart == USART1 )
            m_irqn = USART1_IRQn;
        else if( nameUsart == USART2 )
            m_irqn = USART2_IRQn;
        else if( nameUsart == USART3 )
            m_irqn = USART3_IRQn;
        else if( nameUsart == UART4 )
            m_irqn = UART4_IRQn;
        else if( nameUsart == UART5 )
            m_irqn = UART5_IRQn;
        #if !defined(STM32F1XX)
        else if( nameUsart == USART6 )
            m_irqn = USART6_IRQn;
        #endif
        else    //err
            m_irqn = (IRQn_Type)0;
    }
    return( m_irqn );
}

#if !defined(STM32F1XX)
uint8_t TplUsart::getAF(void)
{
    if( m_af == _AF_STARTUP )   {
        if( nameUsart == USART1 )
            m_af = GPIO_AF_USART1;
        else if( nameUsart == USART2 )
            m_af = GPIO_AF_USART2;
        else if( nameUsart == USART3 )
            m_af = GPIO_AF_USART3;
        else if( nameUsart == UART4 )
            m_af = GPIO_AF_UART4;
        else if( nameUsart == UART5 )
            m_af = GPIO_AF_UART5;
        else if( nameUsart == USART6 )
            m_af = GPIO_AF_USART6;
        else    //error
            m_af=0;
    }
    return( m_af );
}
#elif defined(STM32F1XX)
uint32_t TplUsart::getRemap(void)
{
    if( m_remap == _REMAP_STARTUP )   {
        if( nameUsart == USART1 )
            m_remap = GPIO_Remap_USART1;
        else if( nameUsart == USART2 )
            m_remap = GPIO_Remap_USART2;
        else    //error
            m_remap=0;
    }
    return( m_remap );
}

uint32_t TplUsart::getPartialRemap(void)
{
    if( m_remap == _REMAP_STARTUP )   {
        if( nameUsart == USART3 )
            m_remap = GPIO_PartialRemap_USART3;
        else    //error
            m_remap=0;
    }
    return( m_remap );
}

uint32_t TplUsart::getFullRemap(void)
{
    if( m_remap == _REMAP_STARTUP )   {
        if( nameUsart == USART3 )
            m_remap = GPIO_FullRemap_USART3;
        else    //error
            m_remap=0;
    }
    return( m_remap );
}
#endif

void TplUsart::setDmaTx(bool state)
{
    if (state == true)
    nameUsart->CR3 |= USART_CR3_DMAT;
    else
    nameUsart->CR3 &= ~USART_CR3_DMAT;
}


void TplUsart::setDmaRx(bool state)
{
    if (state == true)
    nameUsart->CR3 |= USART_CR3_DMAR;
    else
    nameUsart->CR3 &= ~USART_CR3_DMAR;
}

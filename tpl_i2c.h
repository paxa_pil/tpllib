#ifndef TPL_I2C_H_INCLUDED
#define TPL_I2C_H_INCLUDED

#include "tpl_std_incl.h"
#include "tpl_pinout.h"
#include "tpl_roundbuffer.h"

#define I2C_FLAG_TIMEOUT_         ((uint32_t)0x1000)
#define I2C_LONG_TIMEOUT         ((uint32_t)(10 * I2C_FLAG_TIMEOUT_))

typedef struct structI2C
{
    I2C_TypeDef* n;
    Pin scl;
    Pin sda;
}I2cHw;

class TplI2c {
    private:
        I2cHw mi2c;
        I2C_InitTypeDef msettings;

        uint8_t (*merrfunc)(uint8_t);
//        uint8_t* mbuffer_in;
//        uint8_t* mbuffer_out;
//        uint32_t mbuffer_size;
    public:
//        TplRoundBuffer rb_in, rb_out;
        uint8_t init( I2cHw i2c, I2C_InitTypeDef settings, uint8_t (*errfunc)(uint8_t) );
        uint8_t writeDevRegister( uint8_t deviceAddr, uint8_t registerAddr,
                                 uint8_t registerValue );
        uint8_t readDevRegister(uint8_t deviceAddr, uint8_t registerAddr);
        //void readIfError(void);
        void inIrqErr(void);
        uint8_t readDevBlock(uint8_t deviceAddr, uint8_t registerAddr, uint8_t *data, uint8_t size);

};

#endif /* TPL_I2C_H_INCLUDED */

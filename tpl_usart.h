#ifndef __TPL_USART_H__
#define __TPL_USART_H__

#include "tpl_std_incl.h"
#include "tpl_pinout.h"
#include "tpl_roundbuffer.h"

//example
//#define UZNX_USART_SETTINGS { 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, (USART_Mode_Rx | USART_Mode_Tx), USART_HardwareFlowControl_None }
//#define USART { USART3, .tx = PA1, .rx = PA2 }

typedef struct structUASRT
{
    USART_TypeDef * n;
    hwPin  tx;
    hwPin  rx;
}hwUsart;

class TplUsart {
	private:
		USART_TypeDef *nameUsart;
		IRQn_Type m_irqn;
		uint8_t m_af;
		uint32_t m_remap = _REMAP_STARTUP;
	public:
		TplRoundBuffer rb_in, rb_out;

		TplUsart(void) {};
		TplUsart( hwUsart usart, USART_InitTypeDef settings, uint32_t sizes)
		{
		    init( usart, settings, sizes, sizes);
		}

		TplUsart( hwUsart usart, USART_InitTypeDef settings, uint32_t sizeIn,
                        uint32_t sizeOut)
		{
		    init( usart, settings, sizeIn, sizeOut);
		}

        uint8_t init( hwUsart usart, USART_InitTypeDef settings,
                       uint32_t sizeIn, uint32_t sizeOut);

		uint8_t init( hwUsart usart, USART_InitTypeDef settings,
               uint8_t* buffer_in, uint8_t* buffer_out, uint32_t buffer_size);

        uint8_t inithw( hwUsart usart, USART_InitTypeDef settings );

        IRQn_Type getIRQn(void);

        #if !defined(STM32F1XX)
            uint8_t getAF(void);
        #elif defined(STM32F1XX)
            uint32_t getRemap(void);
            uint32_t getPartialRemap(void);
            uint32_t getFullRemap(void);
        #endif

		inline void set_irqTXE( bool en ) {
			USART_ITConfig( nameUsart,  USART_IT_TXE, en?ENABLE:DISABLE);
		}
		inline void set_irqRXNE( bool en ) {
			USART_ITConfig( nameUsart,  USART_IT_RXNE, en?ENABLE:DISABLE);
		}
		inline void set_irqTC( bool en ) {
			USART_ITConfig( nameUsart,  USART_IT_TC, en?ENABLE:DISABLE);
		}
		inline void clear_irqTC( void ) {
			USART_ClearITPendingBit( nameUsart, USART_IT_TC);
		}
		inline bool get_irqRXNE(void)	{
			return( (bool)USART_GetITStatus( nameUsart , USART_IT_RXNE) );
		}
		inline bool get_irqTXE(void)	{
			return( (bool)USART_GetITStatus( nameUsart , USART_IT_TXE) );
		}
		inline bool get_irqTC(void)	{
			return( (bool)USART_GetITStatus( nameUsart , USART_IT_TC) );
		}
		inline void start_send(void) {
			set_irqTXE( true );
		}
		inline void inirq(void)
		{
			if( get_irqRXNE() )
			{
				rb_in.push( get_byte() );
			}
			if( get_irqTXE() )
			{
				if( rb_out.getCount() )
					put_byte( rb_out.pop() );
				else
                    set_irqTXE( false );
			}
		}

		void setDmaTx(bool);
		void setDmaRx(bool);
#if defined(STM32F30X)
		inline uint8_t get_byte(void) {
			return( nameUsart->RDR & (uint8_t)0xFF );
		}
		inline void put_byte( uint8_t byte ) {
			nameUsart->TDR = byte;
		}
#elif defined(STM32F1XX) || defined(STM32F2XX) || defined(STM32F4XX)
		inline uint8_t get_byte(void) {
			return( nameUsart->DR & (uint8_t)0xFF );
		}
		inline void put_byte( uint8_t byte ) {
			nameUsart->DR = (byte & (uint16_t)0x01FF);
		}
#else
		#error "Error in tpl_usart.h for this dev"
#endif
};


#endif //__TPL_USART_H__

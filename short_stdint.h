#ifndef SHORT_STDINT_H_INCLUDED
#define SHORT_STDINT_H_INCLUDED

#include "stdint.h"

typedef int8_t i8 ;
typedef uint8_t u8 ;
typedef int16_t i16 ;
typedef uint16_t u16 ;
typedef int32_t i32 ;
typedef uint32_t u32 ;


#endif /* SHORT_STDINT_H_INCLUDED */

#ifndef TPL_IWDG_H_INCLUDED
#define TPL_IWDG_H_INCLUDED

#include "tpl_std_incl.h"

#define TPL_LSI_FREQ        32000

void Tpl_InitIWdg( uint8_t prescaler, uint16_t reload )
{
    RCC_LSICmd( ENABLE);
    IWDG_WriteAccessCmd( IWDG_WriteAccess_Enable);
    IWDG_SetPrescaler( prescaler);
    IWDG_SetReload( reload);
    IWDG_ReloadCounter();
    IWDG_Enable();
}


#endif /* TPL_IWDG_H_INCLUDED */

 #include "tpl_gettime.h"
 #include "system_stm32f2xx.h"

 volatile uint32_t cnt_ms_global;

//work ok
//	1 extern "C" {}
//	2 extern "C" void SysTick_Handler(void)
//  3 extern "C" in declaration in .h and include this file

void SysTick_Handler(void)
{
	cnt_ms_global++;
}


void TplResetTime(void)
{
    cnt_ms_global = 0;
}


void TplInit_GetTime_SysTick(void)
{
    cnt_ms_global = 0;
    SysTick_Config(SystemCoreClock / 1000);
}

uint32_t TplGetTime_ms(void)
{
	return( cnt_ms_global );
}

void delay_ms(uint16_t milliseconds)
{
    uint32_t ct = cnt_ms_global;
    while ( (cnt_ms_global - ct) < milliseconds)
		__asm("nop");
}

//extern "C"  {
void delay_nop( uint32_t nCount)
{
	while(nCount--)
		__asm("nop"); // do nothing
}


void TplTimer_ms::init( tTimerType type_in, uint32_t timeout_in )
{
	m_type = type_in;
	m_cnt_save = cnt_ms_global;
	m_timeout_save = timeout_in;
	if( m_type == TPL_CYCLE_TIMER)
		m_run = true;
	else if( m_type == TPL_ONE_TIMEOUT)
		m_run = false;
}
bool TplTimer_ms::timeout()
{
	if( m_run && (( cnt_ms_global - m_cnt_save) >= m_timeout_save ) )	{
		if( m_type == TPL_CYCLE_TIMER ) {
			m_cnt_save = cnt_ms_global;
			return( true );
		}
		else if(m_type == TPL_ONE_TIMEOUT )	{
			m_run = false;
			return(true);
		}
	}
	return( false);

}
bool TplTimer_ms::start(void)
{
	m_cnt_save = cnt_ms_global;
	m_run = true;
	if( m_type != TPL_ONE_TIMEOUT)
		return(false);
	return(true);
}
bool TplTimer_ms::stop(void)
{
	m_run = false;
	 if( m_type != TPL_ONE_TIMEOUT)
		return(false);
	 return(true);
}

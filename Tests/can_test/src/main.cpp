#include "stm32f4xx_conf.h"
#include "tpl_can.h"
#include "tpl_pinout.h"
#include "tpl_gettime.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef STM32F40_41xxx
    #include "stm32f4_discovery.h"

    #define CAN_PERIPH CAN1
    #define CAN_PIN_TX PB9
    #define CAN_PIN_RX PB8

    #define CAN_DATA_LENGHT 8
    #define CAN_FRAME_ID 0x10
#endif

#ifdef STM32F429_439xx
    #include "stm32f429i_discovery.h"

    #define CAN_PERIPH CAN2
    #define CAN_PIN_TX PB6
    #define CAN_PIN_RX PB5

    #define CAN_DATA_LENGHT 8
    #define CAN_FRAME_ID 0x20
#endif

#define KEY_PRESSED     0x00
#define KEY_NOT_PRESSED 0x01

TplCan *pCan;

extern "C" {
    void CAN1_RX0_IRQHandler(void);
    void CAN1_RX1_IRQHandler(void);
    void CAN2_RX0_IRQHandler(void);
    void CAN2_RX1_IRQHandler(void);

    void receivedPrint(FrameCan frame);
}

int main(void)
{
    uint8_t message[CAN_DATA_LENGHT] = "ABCDEFG";

    /*configure gpio*/
    HwCan hwCan;
    hwCan.n = CAN_PERIPH;
    hwCan.tx = CAN_PIN_TX;
    hwCan.rx = CAN_PIN_RX;

    /*configure can*/
    ConfCan confCan;
    confCan.SJW = CAN_SJW_1tq;
    confCan.BS1 = CAN_BS1_13tq;
    confCan.BS2 = CAN_BS2_2tq;
    confCan.Prescaler = 21;

    pCan = new TplCan(hwCan, confCan);
    //Can can(hwCan, confCan);

    /*configure frame filter*/
    FilterCan filterCan;
#ifdef STM32F429_439xx
    filterCan.num = 15;
    filterCan.id = 0x10;
#endif
#ifdef STM32F40_41xxx
    filterCan.num = 0;
    filterCan.id = 0x20;
#endif
    filterCan.fifo = 0;
    filterCan.state = ENABLE;
    pCan->setFilter(filterCan);

    /*configure transmit frame*/
    FrameCan frameCan;
    frameCan.id = CAN_FRAME_ID;
    memcpy(frameCan.data, message, CAN_DATA_LENGHT);
//    frameCan.data = message;
    frameCan.length = CAN_DATA_LENGHT;

    /*configure test button*/
    STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);

    /*configure leds*/
    STM_EVAL_LEDInit(LED3);
    STM_EVAL_LEDInit(LED4);
    STM_EVAL_LEDOn(LED3);
    STM_EVAL_LEDOn(LED4);

    TplInit_GetTime_SysTick();

    while(1)
    {
        while(STM_EVAL_PBGetState(BUTTON_USER) == KEY_NOT_PRESSED)
        {
            for (uint8_t i=0; i<100; i++)
            {
                pCan->doSend(frameCan);
                delay_ms(100);
                STM_EVAL_LEDToggle(LED3);
            }

            while(STM_EVAL_PBGetState(BUTTON_USER) == KEY_NOT_PRESSED)
            {
            }
        }
    }
}

void CAN1_RX0_IRQHandler(void)
{
    FrameCan frame;
    frame = pCan->doReceive(CAN1, CAN_FIFO0);
    STM_EVAL_LEDToggle(LED4);
    receivedPrint(frame);
}

void CAN1_RX1_IRQHandler(void)
{
    FrameCan frame;
    frame = pCan->doReceive(CAN1, CAN_FIFO1);
    STM_EVAL_LEDToggle(LED4);
    receivedPrint(frame);
}

void CAN2_RX0_IRQHandler(void)
{
    FrameCan frame;
    frame = pCan->doReceive(CAN2, CAN_FIFO0);
    STM_EVAL_LEDToggle(LED4);
    receivedPrint(frame);
}

void CAN2_RX1_IRQHandler(void)
{
    FrameCan frame;
    frame = pCan->doReceive(CAN2, CAN_FIFO1);
    STM_EVAL_LEDToggle(LED4);
    receivedPrint(frame);
}

void receivedPrint(FrameCan frame)
{
    char data[10] = {0};
    memcpy(data, frame.data, 8);
    data[9] = '\0';
    printf("Receive\n.");
    printf("ID: %ld\n", frame.id);
    printf("Data: %s\n", data);
    fflush(stdout);
}

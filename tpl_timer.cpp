#include "tpl_timer.h"
#include "tpl_convert_stm32_std.h"

void TplTimer::init(TIM_TypeDef *_tim, tTimer settings)
{
    tim = _tim;
    m_af = _AF_STARTUP;
    uint32_t apbFreq = Tpl_initDevClock( tim);

    TIM_TimeBaseInitTypeDef  timeBaseStructure;
    timeBaseStructure.TIM_Prescaler =
        (uint16_t) ( (apbFreq*2)/settings.freq ) - 1;
    timeBaseStructure.TIM_Period = settings.period;
    timeBaseStructure.TIM_ClockDivision = settings.division;
    timeBaseStructure.TIM_CounterMode = settings.countMode;
    timeBaseStructure.TIM_RepetitionCounter = settings.repetitionCounter;

    TIM_TimeBaseInit(tim, &timeBaseStructure);
}


#if !defined(STM32F1XX)
uint8_t TplTimer::getAF(void)
{
    #define _IF_TIM_AF(n) if( tim == TIM##n ) m_af=GPIO_AF_TIM##n
    if( m_af == _AF_STARTUP )   {
       _IF_TIM_AF(1);
       else _IF_TIM_AF(2);
       else _IF_TIM_AF(3);
       else _IF_TIM_AF(4);
       else _IF_TIM_AF(5);
       //else _IF_TIM_AF(6);
       //else _IF_TIM_AF(7);
       else _IF_TIM_AF(8);
       else _IF_TIM_AF(9);
       else _IF_TIM_AF(10);
       else _IF_TIM_AF(11);
       else _IF_TIM_AF(12);
       else _IF_TIM_AF(13);
       else _IF_TIM_AF(14);
       else //error
           m_af=0;
    }
    return( m_af );
}
#endif

int TplTimerChannel::init( TplTimer *timer, uint8_t channel )
{
    if( channel> _TPL_TIM_CHANNELS_N || channel<1)
        return(0);

    m_timer = timer;
    m_OCxInit = _TIM_OCnInit[channel-1];
    m_OCxPreloadConfig = _TIM_OCnPreloadConfig[channel-1];
    m_SetCompareX = _TIM_SetCompareN[channel-1];
    return(1);
}


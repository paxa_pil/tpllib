#ifndef __TPL_CONVERT_H__
#define __TPL_CONVERT_H__

#include "tpl_std_incl.h"


#define _AVG_SLOPE  2.5
#define _V25        760.0
#define _KOFF_A     ((3300.0/0xFFF)/_AVG_SLOPE)
#define _KOFF_B     (25-(_V25/_AVG_SLOPE))
#define TEMP_MCU_FROM_ADC(a)  ( _KOFF_A*a + _KOFF_B)

//namespace SplConvertion  {
#define  _AF_STARTUP  0xFF
#define  _REMAP_STARTUP  0xFFFFFFFF
#define _IRQ_STARTUP  127

//#ifdef __cplusplus
//    extern "C"  {
//#endif

    typedef struct {
        bool on;
        uint8_t PreemptionPriority;
        uint8_t SubPriority;
    }tIrqSettings;
#define DEFAULT_IRQ_SETTINGS    {true,0,0}


    uint32_t Tpl_initDevClock( void* device);

    uint8_t Tpl_sourceFromPin(uint16_t pin);

    uint8_t Tpl_irqEnable(  IRQn_Type IRQn,
                          tIrqSettings settings=DEFAULT_IRQ_SETTINGS);


    typedef void (*pirq)(void);

    uint8_t Tpl_getExtiPortSource( GPIO_TypeDef *port);

//#ifdef __cplusplus
//    }
//#endif

//}


#endif //__PLIB_CONVERT_H__

#include "tpl_adc_dma.h"
#include "string.h"

void TplAdcDma::init(ADC_TypeDef *adc, uint8_t *channels, uint8_t numChannels, hwDmaForAdc* pDma, bool regular)
{
	mNumChannels = numChannels;
    mDma = pDma;
    mAdc = adc;
    mChannels = channels;
    mAdcData = new uint16_t[mNumChannels];
    memset(mAdcData, 0x00, (mNumChannels * sizeof(uint16_t)));
    mRegular = regular;

    Tpl_initDevClock(mAdc);
    Tpl_initDevClock(DMA2);

    configDma();

    ADC_CommonInitTypeDef ADC_CommonInitStructure;
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

    configAdc();

    ADC_DMARequestAfterLastTransferCmd(mAdc, ENABLE);
    ADC_DMACmd(mAdc, ENABLE);
    ADC_Cmd(mAdc, ENABLE);

    if(mRegular)
		ADC_SoftwareStartConv(mAdc);
}

void TplAdcDma::configDma()
{
	DMA_InitTypeDef DMA_InitStructure;

	DMA_InitStructure.DMA_Channel = mDma->channel;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)mAdcData;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(mAdc->DR) );
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = mNumChannels;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(mDma->stream, &DMA_InitStructure);

	DMA_Cmd(mDma->stream, ENABLE);
}

void TplAdcDma::configAdc()
{
    ADC_InitTypeDef ADC_InitStructure;

    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = (mRegular?ENABLE:DISABLE);
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = mNumChannels;
    ADC_Init(mAdc, &ADC_InitStructure);

    for(int i = 0; i < mNumChannels; i++)
        ADC_RegularChannelConfig(mAdc, mChannels[i], i+1, ADC_SampleTime_480Cycles);
}
#ifndef __TPL_STD_INCL_H__
#define __TPL_STD_INCL_H__

#include "short_stdint.h"

#if defined(STM32F40_41xxx) || defined(STM32F427_437xx) || defined(STM32F429_439xx) || defined(TM32F401xx)
    #define STM32F4XX
#endif

#if defined(STM32F205xx) || defined(STM32F215xx) || defined(STM32F207xx) || defined(STM32F217xx)
    #define STM32F2XX
#endif

#if !defined(TPL_USE_HAL)
    #if defined(STM32F2XX)
        #include "stm32f2xx.h"
        #include "stm32f2xx_conf.h"
        #define STD_GPIO_MaxSpeed GPIO_Speed_100MHz
    #elif defined(STM32F4XX)
        #define STD_GPIO_MaxSpeed GPIO_Speed_100MHz
        #include "stm32f4xx.h"
        #include "stm32f4xx_conf.h"
    #elif defined(STM32F30X)
        #include "stm32f30x.h"
        #include "stm32f30x_conf.h"
        #define STD_GPIO_MaxSpeed	GPIO_Speed_Level_3
    #elif defined(STM32F10X_MD)
        #include "stm32f10x.h"
        #include "stm32f10x_conf.h"
        #define STD_GPIO_MaxSpeed	GPIO_Speed_50MHz
    #else
        #error "Please select first the target for tpl_stm32"
    #endif
#else
     #if defined(STM32F2XX)
        #include "stm32f2xx.h"
        #include "stm32f2xx_hal_conf.h"
       // #define STD_GPIO_MaxSpeed GPIO_Speed_100MHz
    #else
        #error "Please select first the target for tpl_stm32"
    #endif
#endif //TPL_USE_HAL

#endif //__TPL_STD_INCL_H__


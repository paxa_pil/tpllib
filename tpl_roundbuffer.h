#pragma once

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "stdint.h"

#define TPL_PRINTF_MAX_LENGHT	256

class TplRoundBuffer
{
	private:
		uint32_t mtail;
		uint32_t mhead ;
        uint8_t *mbuf;
		uint32_t msize;

		void mrb_plus( uint32_t *par, int32_t plus )
		{
			if( plus<0 && (*par<(uint32_t)-plus ) )	{
				*par= *par + msize + plus;
				return;
			}
			*par = *par + plus;
			if(*par >=	msize )
				*par-= msize;
		}

	public:
		/**
		@brief Initializes a new instance of the TplRoundBuffer class
               that is empty and has no initial size.
		*/
		TplRoundBuffer(void) { }
		/**
		@brief Initializes a new instance of the TplRoundBuffer class
               that is empty and has the specified initial size.
		@param size The number of bytes that the new TplRoundBuffer can initially store.
		*/
		TplRoundBuffer(uint32_t size);
		/**
		@brief Initializes a new instance of the TplRoundBuffer class
               that is empty and has the specified initial size.
		@param size The number of bytes that the new TplRoundBuffer can initially store.
		*/
        void init(uint32_t size);
		/**
		@brief Initializes a new instance of the TplRoundBuffer class
               that internally points to external buffer and has the specified initial size.
		@param buffer External buffer the new TplRoundBuffer should points to.
		@param size The number of bytes that the new TplRoundBuffer can initially store.
		*/
        void init(void *buffer, uint32_t size);
		/**
		@brief Removes all bytes from the TplRoundBuffer.
		*/
        void clear(void);
		/**
		@brief Removes a range of bytes from the top of the TplRoundBuffer.
		@param count The number of bytes to remove.
		@return flase if empty after removing; otherwise, true.
		*/
        bool removeRange(uint32_t count);
		/**
		@brief Gets the number of bytes actually contained in the TplRoundBuffer.
		@return The number of bytes actually contained in the TplRoundBuffer.
		*/
        uint32_t getCount(void);
        /**
		@brief Gets the number of bytes contained between head and index in the TplRoundBuffer.
		@return The number of bytes contained between head and index in the TplRoundBuffer.
		*/
        uint32_t getCount(uint32_t index);
		/**
		@brief Gets the byte at the specified index.
		@param index The zero-based index of the byte to get.
		@return The byte at the specified index.
		*/
        uint8_t operator [](uint32_t index);
		/**
		@brief Gets two bytes starting from the specified index.
		@param index The zero-based index of the first byte to get.
		@return Two bytes starting from the specified index.
		*/
        uint16_t getWordUnsafe(uint32_t index);
		/**
		@brief Copies a range of bytes from the TplRoundBuffer to the external buffer.
		@param resultBuffer The external buffer.
		@param index The zero-based starting index of the range of bytes to copy.
		@param count The number of bytes to copy.
		@return The number of actually copied bytes.
		*/
		uint32_t get(void *resultBuffer, uint32_t index, uint32_t count);
		/**
		@brief Adds a byte to the end of the TplRoundBuffer.
		@param byte The byte to add to the TplRoundBuffer.
		@return false if overflow; otherwise, true.
		*/
        bool push(uint8_t byte);
		/**
		@brief Adds a range of bytes from the top of the specified external buffer
		       to the end of the TplRoundBuffer.
		@param buffer The external buffer.
		@param count The number of bytes to add.
		@return false if overflow; otherwise, true.
		*/
		bool push(void *buffer, uint32_t count);
		/**
		@brief Add a byte to the specified index of the current TplRoundBuffer.
		@param index The zero-based starting index of the byte to push.
		@param byte The byte to add to the TplRoundBuffer.
		@return false if overflow; otherwise, true.
		*/
		bool push(uint32_t index, uint8_t byte);
		/**
		@brief Adds a range of bytes from the top of the specified TplRoundBuffer
		       to the end of the current TplRoundBuffer.
		@param tplRoundBuffer The external TplRoundBuffer.
		@param count The number of bytes to add.
		@return false if overflow; otherwise, true.
		*/
		bool push(TplRoundBuffer *tplRoundBuffer, uint32_t count);
		/**
		@brief Removes and returns the byte at the beginning of the TplRoundBuffer.
		@return The byte at the beginning of the TplRoundBuffer.
		*/
        uint8_t pop(void);
		/**
		@brief Removes and returns the byte at the beginning of the TplRoundBuffer.
		@return The byte at the beginning of the TplRoundBuffer.
		*/
        uint8_t popUnsafe(void);
		/**
		@brief Removes a range of bytes from the beginning of the TplRoundBuffer and adds it
		       to the specified external buffer.
		@param resultBuffer The external buffer.
		@param count The number of bytes to remove.
		@return The number of actually removed bytes.
		*/
		uint32_t pop(void *resultBuffer, uint32_t count);
		/**
		@brief
		@param
		@param
		@return 0 if ok
		*/
		int memcmp(void *buffer, uint32_t sizebuf);
		/**
		@brief
		@param
		@param
		@return >=0 - the begin of mem; <0 - negative
		*/
		int memSearch(void *buffer, uint32_t sizebuf);
		/**
		@brief
		@param
		@param
		*/
        int32_t findPackage(uint8_t *startPattern, uint8_t sizePattern, uint32_t sizePackage);
		/**
		@brief
		@param
		@param
		*/
		void printf(char *format, ...);
		/**
		@brief
		@param
		@return
		*/
		char* printfBuffer(char *str);
};

char* PL_PrintfBuffer(void *buf, uint8_t buf_len, char *str);

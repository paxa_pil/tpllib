#include "tpl_i2c.h"
#include "tpl_gettime.h"

uint8_t TplI2c::init( I2cHw i2c, I2C_InitTypeDef settings, uint8_t (*errfunc)(uint8_t) )
{
    mi2c = i2c;
    msettings = settings;
    merrfunc = errfunc;
//	mbuffer_in = buffer_in;
//	mbuffer_out= buffer_out;
//	mbuffer_size = buffer_size;

//	rb_in.init( mbuffer_in, mbuffer_size );
//	rb_out.init( mbuffer_out, mbuffer_size );
    TPL_RCC_APBx_InitDevClock(mi2c.n);

    #if defined(STM32F10X_MD)
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE);
        TplPin scl( mi2c.scl , PIN_SET_AF_OD );
        TplPin sda( mi2c.sda , PIN_SET_AF_OD );

        I2C_DeInit( mi2c.n);
        I2C_Init( mi2c.n, &msettings);
        I2C_ITConfig( mi2c.n, I2C_IT_ERR, ENABLE);
        I2C_Cmd( mi2c.n, ENABLE);
    #else
        #error "Error in func TplI2c::init for this dev"
    #endif
}

/*
uint8_t TplI2c::writeByte(uint8_t byte)
{
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(sEE_I2C, I2C_FLAG_BUSY))
    {
        if((mi2cTimeout--) == 0) return 1;
    }
    I2C_SendData(mi2c.n, byte);
}
*/
/** \brief Read register from dev
 *
 * \param deviceAddr:
 * \param registerAddr:
 * \param registerValue:
 * \return 1 if OK, 0 if error, see error in merrfunc
 *
 */
uint8_t TplI2c::writeDevRegister( uint8_t deviceAddr, uint8_t registerAddr,
                                 uint8_t registerValue )
{
   /* mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_BUSY))
    {
        if((mi2cTimeout--) == 0) return 1;
    }
    */
    /* Enable the I2C peripheral */
    uint32_t mi2cTimeout;

    I2C_GenerateSTART(mi2c.n, ENABLE);
    /* Test on SB Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_SB) == RESET)
    {
        if((mi2cTimeout--) == 0) {
            I2C_GenerateSTOP(mi2c.n, ENABLE);
            return(merrfunc(1));
        }
    }
    /* Transmit the slave address and enable writing operation */
    I2C_Send7bitAddress(mi2c.n, deviceAddr, I2C_Direction_Transmitter);
     /* Test on ADDR Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
    {
        if (mi2cTimeout-- == 0) {
            I2C_GenerateSTOP(mi2c.n, ENABLE);
            return(merrfunc(2));
        }
    }

    /* Transmit the first address for r/w operations */
    I2C_SendData(mi2c.n, registerAddr);

    /* Test on TXE FLag (data dent) */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while ((!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_TXE)) &&
           (!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_BTF)))
    {
        if (mi2cTimeout-- == 0) {
            I2C_GenerateSTOP(mi2c.n, ENABLE);
            return(merrfunc(3));
        }
    }

    /* Transmit data */
    I2C_SendData(mi2c.n, registerValue);

    /* Test on TXE FLag (data dent) */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while ((!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_TXE)) &&
           (!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_BTF)))
    {
        if (mi2cTimeout-- == 0) {
            I2C_GenerateSTOP(mi2c.n, ENABLE);
            return(merrfunc(4));
        }
    }
    /* Send STOP Condition */
    I2C_GenerateSTOP(mi2c.n, ENABLE);
    return(1);
}

/** \brief Read register from dev
 *
 * \param deviceAddr:
 * \param registerAddr:
 * \return return value, if error merrfunc callback
 *
 */
uint8_t TplI2c::readDevRegister(uint8_t deviceAddr, uint8_t registerAddr)
{
    uint32_t mi2cTimeout;

    I2C_GenerateSTART(mi2c.n, ENABLE);
    /* Test on SB Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_SB) == RESET)
    {
        if((mi2cTimeout--) == 0)
        {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            //return(merrfunc(5));
            merrfunc(5);
            break;
        }
    }
   // delay_ms(1);

    /* Transmit the slave address and enable writing operation */
    I2C_Send7bitAddress(mi2c.n, deviceAddr, I2C_Direction_Transmitter);
    /* Test on ADDR Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(6);
            break;
        }
    }
  //  delay_ms(1);
    /* Transmit the first address for r/w operations */
    I2C_SendData(mi2c.n, registerAddr);

    /* Test on TXE FLag (data dent) */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while ((!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_TXE)) &&
           (!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_BTF)))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(7);
            break;
            //return(merrfunc(7));
        }
    }

  //  delay_ms(1);
    /* Send START condition a second time */
    I2C_GenerateSTART(mi2c.n, ENABLE);
    /* Test on SB Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_SB) == RESET)
    {
        if((mi2cTimeout--) == 0)    {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(8);
            break;
            //return(merrfunc(8));
        }
    }
   // delay_ms(1);
    I2C_AcknowledgeConfig( mi2c.n, DISABLE);
    /* Send IOExpander address for read */
    I2C_Send7bitAddress(mi2c.n, deviceAddr, I2C_Direction_Receiver);

    /* Test on ADDR Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
         //   return(merrfunc(7));
            merrfunc(9);
            break;
        }
    }


    /* Wait for the byte to be received */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_RXNE) == RESET)
    {
      if((mi2cTimeout--) == 0)  {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(10);
            break;
            //return(merrfunc(8));
      }
    }

    /* Recive data */
    uint8_t data = I2C_ReceiveData(mi2c.n);




    /* Send STOP Condition */
    I2C_GenerateSTOP(mi2c.n, ENABLE);
    return(data);
}

uint8_t TplI2c::readDevBlock(uint8_t deviceAddr, uint8_t registerAddr, uint8_t *data, uint8_t size)
{
    uint32_t mi2cTimeout;


    I2C_GenerateSTART(mi2c.n, ENABLE);
    /* Test on SB Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_SB) == RESET)
    {
        if((mi2cTimeout--) == 0)
        {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            //return(merrfunc(5));
            merrfunc(5);
            break;
        }
    }
   // delay_ms(1);

    /* Transmit the slave address and enable writing operation */
    I2C_Send7bitAddress(mi2c.n, deviceAddr, I2C_Direction_Transmitter);
    /* Test on ADDR Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(6);
            break;
        }
    }
  //  delay_ms(1);
    /* Transmit the first address for r/w operations */
    I2C_SendData(mi2c.n, registerAddr|0x80);

    /* Test on TXE FLag (data dent) */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while ((!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_TXE)) &&
           (!I2C_GetFlagStatus(mi2c.n,I2C_FLAG_BTF)))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(7);
            break;
            //return(merrfunc(7));
        }
    }

  //  delay_ms(1);
    /* Send START condition a second time */
    I2C_GenerateSTART(mi2c.n, ENABLE);
    /* Test on SB Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_SB) == RESET)
    {
        if((mi2cTimeout--) == 0)    {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
            merrfunc(8);
            break;
            //return(merrfunc(8));
        }
    }
   // delay_ms(1);
    I2C_AcknowledgeConfig( mi2c.n, ENABLE);
    /* Send IOExpander address for read */
    I2C_Send7bitAddress(mi2c.n, deviceAddr, I2C_Direction_Receiver);

    /* Test on ADDR Flag */
    mi2cTimeout = I2C_LONG_TIMEOUT;
    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
    {
        if (mi2cTimeout-- == 0)     {
            //I2C_GenerateSTOP(mi2c.n, ENABLE);
         //   return(merrfunc(7));
            merrfunc(9);
            break;
        }
    }

    uint8_t reg_cnt=size;
    uint8_t *pdata = data;
    while(reg_cnt>0)
    {
        reg_cnt--;
        if(!reg_cnt)
            I2C_AcknowledgeConfig( mi2c.n, DISABLE);
        /* Wait for the byte to be received */
        mi2cTimeout = I2C_LONG_TIMEOUT;
        while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_RXNE) == RESET)
        {
          if((mi2cTimeout--) == 0)  {
                //I2C_GenerateSTOP(mi2c.n, ENABLE);
                merrfunc(10);
                break;
                //return(merrfunc(8));
          }
        }

        /* Recive data */
        *pdata = I2C_ReceiveData(mi2c.n);
        pdata++;
    }
    /* Send STOP Condition */
    I2C_GenerateSTOP(mi2c.n, ENABLE);
    return(data);
}


void TplI2c::inIrqErr(void)
{
/* Check on I2C2 SMBALERT flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_SMBALERT))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_SMBALERT);
   // SMbusAlertOccurred++;
  }
  /* Check on I2C2 Time out flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_TIMEOUT))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_TIMEOUT);
  }
  /* Check on I2C2 Arbitration Lost flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_ARLO))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_ARLO);
  }

  /* Check on I2C2 PEC error flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_PECERR))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_PECERR);
  }
  /* Check on I2C2 Overrun/Underrun error flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_OVR))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_OVR);
  }
  /* Check on I2C2 Acknowledge failure error flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_AF))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_AF);
  }
  /* Check on I2C2 Bus error flag and clear it */
  if (I2C_GetITStatus(mi2c.n, I2C_IT_BERR))
  {
    I2C_ClearITPendingBit(mi2c.n, I2C_IT_BERR);
  }
}


//void TplI2c::readIfError(void)
//{
//    uint32_t mi2cTimeout;
//    I2C_AcknowledgeConfig( mi2c.n, DISABLE);
//    /* Send IOExpander address for read */
//    I2C_Send7bitAddress(mi2c.n, 0, I2C_Direction_Receiver);
//    /* Test on ADDR Flag */
//    mi2cTimeout = I2C_LONG_TIMEOUT;
//    while (!I2C_CheckEvent(mi2c.n, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
//    {
//        if (mi2cTimeout-- == 0)     {
//            //I2C_GenerateSTOP(mi2c.n, ENABLE);
//         //   return(merrfunc(7));
//            merrfunc(7);
//            break;
//        }
//    }
//
//
//    /* Wait for the byte to be received */
//    mi2cTimeout = I2C_LONG_TIMEOUT;
//    while(I2C_GetFlagStatus(mi2c.n, I2C_FLAG_RXNE) == RESET)
//    {
//      if((mi2cTimeout--) == 0)  {
//            //I2C_GenerateSTOP(mi2c.n, ENABLE);
//            merrfunc(8);
//            break;
//      }
//    }
//
//    /* Recive data */
//    uint8_t data = I2C_ReceiveData(mi2c.n);
//    /* Send STOP Condition */
//    I2C_GenerateSTOP(mi2c.n, ENABLE);
//}

#include "tpl_rs485.h"

TplRS485::TplRS485(hwUsart usart, USART_InitTypeDef settings, uint32_t sizes, hwPin pinDE)
{
    rb_in.init(sizes);
    rb_out.init(sizes);

    usartDE.init(pinDE, PIN_SET_OUT);
    usartDE.set(false);

    inithw(usart, settings);
}

uint8_t TplRS485::inithw(hwUsart usart, USART_InitTypeDef settings)
{
    nameUsart = usart.n;

    m_af = _AF_STARTUP;
    m_irqn = (IRQn_Type)_IRQ_STARTUP;

    #if defined(STM32F10X_MD)
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE);
    #endif

    if(!Tpl_initDevClock( nameUsart) )
        return(0);

    #if defined(STM32F30X) || defined(STM32F2XX) || defined(STM32F4XX)
        if(usart.rx.pin)
        {
            TplPin rx( usart.rx, PIN_SET_AF );
            rx.setAF(getAF());
        }

        if(usart.tx.pin)
        {
            TplPin tx( usart.tx, PIN_SET_AF );
            tx.setAF(getAF());
        }
    #elif defined(STM32F10X_MD)
        if(usart.rx.pin)
        {
            TplPin rx( usart.rx, PIN_SET_IN_NOPULL );
            rx.setAF(getAF());
        }

        if(usart.tx.pin)
        {
            TplPin tx( usart.tx, PIN_SET_AF_PP );
            tx.setAF(getAF());
        }

    #else
        #error "No func TplUsart::init for this stm32"
    #endif

    USART_Init(nameUsart, &settings);
    USART_Cmd(nameUsart, ENABLE);

    NVIC_EnableIRQ( getIRQn() );

    if(usart.rx.pin)
        set_irqRXNE(true);

    return(1);
}

IRQn_Type TplRS485::getIRQn(void)
{
    if(m_irqn == _IRQ_STARTUP)
    {
        if( nameUsart == USART1 )
            m_irqn = USART1_IRQn;
        else if( nameUsart == USART2 )
            m_irqn = USART2_IRQn;
        else if( nameUsart == USART3 )
            m_irqn = USART3_IRQn;
        else if( nameUsart == UART4 )
            m_irqn = UART4_IRQn;
        else if( nameUsart == UART5 )
            m_irqn = UART5_IRQn;
        else if( nameUsart == USART6 )
            m_irqn = USART6_IRQn;
        else    //err
            m_irqn = (IRQn_Type)0;
    }

    return(m_irqn);
}


uint8_t TplRS485::getAF(void)
{
    if( m_af == _AF_STARTUP )
    {
        if( nameUsart == USART1 )
            m_af = GPIO_AF_USART1;
        else if( nameUsart == USART2 )
            m_af = GPIO_AF_USART2;
        else if( nameUsart == USART3 )
            m_af = GPIO_AF_USART3;
        else if( nameUsart == UART4 )
            m_af = GPIO_AF_UART4;
        else if( nameUsart == UART5 )
            m_af = GPIO_AF_UART5;
        else if( nameUsart == USART6 )
            m_af = GPIO_AF_USART6;
        else    //error
            m_af=0;
    }

    return( m_af );
}


#include "tpl_pinout.h"

#ifndef TPL_CAN_H_INCLUDED
#define TPL_CAN_H_INCLUDED

//restriction network by specification
#define MAX_ID 0x07FF
#define MIN_ID 0x0010

#define GPIO_AF_CAN          ((uint8_t)0x09)

#define MAX_DATA 8

typedef struct structCAN
{
    CAN_TypeDef * n;
    hwPin  tx;
    hwPin  rx;
}HwCan;

typedef struct structFRAME
{
    uint32_t id;
    uint8_t  data[8];
    uint8_t  length;
}FrameCan;

typedef struct structCONF
{
    uint8_t  SJW;
    uint8_t  BS1;
    uint8_t  BS2;
    uint16_t Prescaler;
}ConfCan;

typedef struct filterCONF
{
    uint8_t  num;
    uint32_t id;
    uint8_t  fifo;
    FunctionalState  state;
}FilterCan;

class TplCan
{
private:
    HwCan   mhw;
    ConfCan mconf;

    uint8_t doInitialize(HwCan hw, ConfCan conf);
public:
    TplCan(HwCan hw);
    TplCan(HwCan hw, ConfCan conf);

    FrameCan doReceive(CAN_TypeDef*, uint8_t);
    int8_t doSend(FrameCan frame);
    void setFilter(FilterCan filter);
};

#endif /* TPL_CAN_H_INCLUDED */

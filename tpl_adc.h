#ifndef TPL_ADC_H_INCLUDED
#define TPL_ADC_H_INCLUDED

#include "tpl_pinout.h"

class TplAdc {
private:
    ADC_TypeDef *m_adc;
public:
	TplAdc():
		m_adc(nullptr)
	{ }

    void init( ADC_TypeDef *adc, ADC_InitTypeDef settings );

};

#endif /* TPL_ADC_H_INCLUDED */
